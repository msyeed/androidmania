// IKeyGenerator.aidl
package com.example.admin.serviceaidltest;


//MUST: include this
import com.example.admin.serviceaidltest.ParcelabeTest;
// Declare any non-default types here with import statements



interface IKeyGenerator {
    String getKey();
    int getInteger();
    char getChar();
    long getLong();
    boolean getBoolean();
    List<String> getKeyNames();
    Map getKeyMap();
    ParcelabeTest getPercelableRectDimension(int location);
    void setPercelableRectDimension(in ParcelabeTest rect);
    String getMeTest();
}
