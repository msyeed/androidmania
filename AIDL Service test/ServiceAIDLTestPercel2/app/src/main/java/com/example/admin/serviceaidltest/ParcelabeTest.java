package com.example.admin.serviceaidltest;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

/**
 * Created by admin on 15.6.2015.
 */
public class ParcelabeTest implements Parcelable {

    public int left;
    public String top;
    public long right;

    public ParcelabeTest(int lefts, String tops, long rights) {
        this.left = lefts;
        this.top = tops;
        this.right = rights;

        Log.i("Left: ", "" + left);
        Log.i("top: ", "" + top);
        Log.i("right: ", "" + right);
    }

    public int getLeft() {
        return left;
    }

    public String getTop() {
        return top;
    }

    public long getRight() {
        return right;
    }

    public String getDimension(){
        return this.left +"-"+ this.top + "-"+this.right;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt(left);
        dest.writeString(top);
        dest.writeLong(right);

    }

    /**
     * Retrieving Profile data from Parcel object
     * This constructor is invoked by the method createFromParcel(Parcel source) of
     * the object CREATOR
     **/
    private ParcelabeTest(Parcel in){
        this.left = in.readInt();
        this.top = in.readString();
        this.right = in.readLong();

    }

    public static final Parcelable.Creator<ParcelabeTest> CREATOR = new Parcelable.Creator<ParcelabeTest>() {


        @Override
        public ParcelabeTest createFromParcel(Parcel source) {
            return new ParcelabeTest(source);
        }

        @Override
        public ParcelabeTest[] newArray(int size) {
            return new ParcelabeTest[size];
        }
    };
}
