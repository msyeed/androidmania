package com.example.admin.serviceaidltestpercel2;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.admin.serviceaidltest.IKeyGenerator;
import com.example.admin.serviceaidltest.ParcelabeTest;

import java.util.List;
import java.util.Map;


public class MainActivity extends ActionBarActivity {

    protected static final String TAG = "KeyServiceUser";
    private IKeyGenerator mKeyGeneratorService;
    private boolean mIsBound;
    private String keyNames = "";
    private String mapNames = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView output = (TextView) findViewById(R.id.output);
        final TextView listoutput = (TextView) findViewById(R.id.list_textView);
        final TextView mapoutput = (TextView) findViewById(R.id.maptextView);
        final TextView parceloutput = (TextView) findViewById(R.id.parceltextView);


        final Button goButton = (Button) findViewById(R.id.go_button);
        goButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                try {

                    // Call KeyGenerator and get a new ID
                    if (mIsBound) {
                        Log.i("IN client / mIsBound:- ", "" + mIsBound);
                        output.setText(mKeyGeneratorService.getMeTest() + "-" + mKeyGeneratorService.getKey() + "-" +
                                mKeyGeneratorService.getInteger() + "-" +
                                mKeyGeneratorService.getChar() + "-" +
                                mKeyGeneratorService.getBoolean() + "-" +
                                mKeyGeneratorService.getLong());

                        try {
                            //First copy the returned list in local, otherwise it will stall due to excessive numeber of thread creation.
                            List<String> l = mKeyGeneratorService.getKeyNames();

                            for (int i = 0; i < l.size(); i++)
                                keyNames = keyNames + "-" + l.get(i);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }

                        listoutput.setText(keyNames);


                        try {
                            //First copy the returned map in local, otherwise it will stall due to excessive numeber of thread creation.
                            Map<String,Integer> m = mKeyGeneratorService.getKeyMap();

                            for (String key : m.keySet())
                                mapNames = mapNames + "-" + key + ":" + m.get(key);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }

                        mapoutput.setText(keyNames);


                        //send parcelable to the service to store.

                        ParcelabeTest parcelabeTest = new ParcelabeTest(1, "HIGH", 999);
                        mKeyGeneratorService.setPercelableRectDimension(parcelabeTest);


                        parcelabeTest = mKeyGeneratorService.getPercelableRectDimension(0);
                        parceloutput.setText(parcelabeTest.getDimension());

                    }
                    Log.i("IN client / mIsBound:- ", "" + mIsBound);


                } catch (RemoteException e) {

                    Log.e(TAG, e.toString());

                }
            }
        });
    }


    // Bind to KeyGenerator Service
    @Override
    protected void onStart() {
        super.onStart();

        //if (!mIsBound) {

        //Intent intents = new Intent(IKeyGenerator.class.getName());
        //Intent intent = new Intent(IKeyGenerator.class.getName());
        //bindService(intents, this.mConnection, Context.BIND_AUTO_CREATE);

        //bindService(new Intent(IKeyGenerator.class.getName()), mConnection, Context.BIND_AUTO_CREATE);

        //Log.i("Intent package: " , IKeyGenerator.class.getPackage().toString());
        // explicit Intent, safe
        Intent serviceIntent =new Intent(IKeyGenerator.class.getName());
        serviceIntent.setPackage("com.example.admin.serviceaidltest");

        serviceIntent.setAction("service.Keygenerator");
        bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE);


        //Intent serviceIntent =new Intent(this, KeyGeneratorService.class);
        //bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE);



        // }
    }

    // Unbind from KeyGenerator Service
    @Override
    protected void onStop() {

        if (mIsBound) {

            unbindService(this.mConnection);

        }

        super.onStop();
    }

    private final ServiceConnection mConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className, IBinder iservice) {

            mKeyGeneratorService = IKeyGenerator.Stub.asInterface(iservice);

            mIsBound = true;

        }

        public void onServiceDisconnected(ComponentName className) {

            mKeyGeneratorService = null;

            mIsBound = false;

        }
    };
}
