package com.example.admin.serviceaidltest;

import android.app.Service;
import android.content.Intent;
import android.graphics.*;
import android.graphics.Rect;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

/**
 * Created by mahbubul on 11.6.2015.
 */
public class KeyGeneratorService extends Service {
    public KeyGeneratorService() {
    }

    // Set of already assigned IDs
    // Note: These keys are not guaranteed to be unique if the Service is killed
    // and restarted.

    private final static Set<UUID> mIDs = new HashSet<UUID>();
    private final static List<Integer> i = new ArrayList<>();
    private final static List<String> keyList = new ArrayList<>();
    private final static Map<String, Integer> keyMap = new HashMap<String, Integer>();
    public static List<ParcelabeTest> parcelList = new ArrayList<>();


    // Implement the Stub for this Object
    private final IKeyGenerator.Stub mBinder = new IKeyGenerator.Stub() {

        // Implement the remote method
        public String getKey() {

            UUID id;

            // Acquire lock to ensure exclusive access to mIDs
            // Then examine and modify mIDs

            synchronized (mIDs) {

                do {

                    id = UUID.randomUUID();

                } while (mIDs.contains(id));

                mIDs.add(id);
            }
            Log.i("Process id:- ", id.toString());

            return id.toString();
        }

        @Override
        public int getInteger() throws RemoteException {

            int id = 0;
            synchronized (i) {
                do {
                    id = new Random().nextInt(100);
                } while (i.contains(id));
            }
            Log.i("Service/Integer id: ", "" + id);
            return id;
        }

        @Override
        public char getChar() throws RemoteException {
            char c = 'c';

            return c;
        }

        @Override
        public long getLong() throws RemoteException {
            long testLong = (long) 111.1111111;
            return testLong;
        }

        @Override
        public boolean getBoolean() throws RemoteException {

            boolean b = true;
            return b;
        }

        @Override
        public List<String> getKeyNames() throws RemoteException {

            synchronized (keyList) {
                for (int i = 0; i < 10; i++)
                    keyList.add("key_" + i);
            }
            return keyList;
        }

        @Override
        public Map getKeyMap() throws RemoteException {

            synchronized (keyMap) {
                for(int i = 0; i < 10; i++)
                    keyMap.put("Key", i);
            }
            return keyMap;
        }

        @Override
        public ParcelabeTest getPercelableRectDimension(int location) throws RemoteException {

            ParcelabeTest p = new ParcelabeTest(1, "Rajit", 22);

            Log.i("P: ",p.getTop());
            Log.i("P: ","" + p.getLeft());
            Log.i("P: ","" + p.getRight());
            Log.i("P: location ","" + location);
            return p;

        }

        @Override
        public void setPercelableRectDimension(ParcelabeTest rect) throws RemoteException {

            Log.i("Service/set Parcel: ", rect.getDimension());
            parcelList.add(rect);

        }

        @Override
        public String getMeTest() throws RemoteException {
            return "Mahbubul Syeed";
        }

    };


    @Override
    public IBinder onBind(Intent intent) {


        Log.i("IN the service:- ", "" + mBinder.toString());
        return mBinder;

        //return null;

        // TODO: Return the communication channel to the service.
        //throw new UnsupportedOperationException("Not yet implemented");
    }
}
