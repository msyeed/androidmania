// IKeyGenerator.aidl
package com.example.admin.serviceaidltest;

// Declare any non-default types here with import statements
interface IKeyGenerator {
    String getKey();
    int getInteger();
            char getChar();
            long getLong();
            boolean getBoolean();
}

