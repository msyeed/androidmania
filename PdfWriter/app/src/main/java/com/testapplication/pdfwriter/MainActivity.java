package com.testapplication.pdfwriter;

import android.content.Intent;
import android.graphics.pdf.PdfDocument;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button b = (Button) findViewById(R.id.button1);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //createPDF();
                createPdfwithItext();
            }
        });
    }




    private void createPdfwithItext()
    {

        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyhhmmss");
        String pdfName = "pdfdemo"
                + sdf.format(Calendar.getInstance().getTime()) + ".pdf";

// all created files will be saved at path /sdcard/PDFDemo_AndroidSRC/
        String path = Environment.getExternalStorageDirectory() + "/download/";
        File outputFile = new File(path, pdfName);

        File pdfFolder = new File(path, pdfName);
        if (!pdfFolder.exists()) {
            pdfFolder.mkdir();
            Log.i("IText pdf:", "Pdf Directory created");
        }

        //Create time stamp
        Date date = new Date() ;
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(date);

        File myFile = new File(pdfFolder + timeStamp + ".pdf");

        OutputStream output = null;
        try {
            output = new FileOutputStream(myFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        //Step 1
        Document document = new Document();

        //Step 2
        try {
            PdfWriter.getInstance(document, output);
        } catch (DocumentException e) {
            e.printStackTrace();
        }

        //Step 3
        document.open();

        //Step 4 Add content

        try {
            document.add(new Paragraph("I am rajit"));
            document.add(new Paragraph("I am writing new text to this pdf. please write it!"));
        } catch (DocumentException e) {
            e.printStackTrace();
        }

        //Step 5: Close the document
        document.close();

    }



















    public void createPDF()
    {
 /*       // Create a object of PdfDocument
        PdfDocument document = new PdfDocument();

// content view is EditText for my case in which user enters pdf content
        View content = findViewById(R.id.textview);

// crate a page info with attributes as below
// page number, height and width
// i have used height and width to that of pdf content view
        int pageNumber = 1;
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(content.getWidth(),
                content.getHeight() - 20, pageNumber).create();

// create a new page from the PageInfo
        PdfDocument.Page page = document.startPage(pageInfo);

// repaint the user's text into the page
        content.draw(page.getCanvas());

// do final processing of the page
        document.finishPage(page);*/

        // Create a shiny new (but blank) PDF document in memory
        PdfDocument document = new PdfDocument();

        // crate a page description
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(300, 300, 1).create();

        // create a new page from the PageInfo
        PdfDocument.Page page = document.startPage(pageInfo);

        // repaint the user's text into the page
        View content = findViewById(R.id.tablelayout);
        content.draw(page.getCanvas());

        // do final processing of the page
        document.finishPage(page);



// saving pdf document to sdcard
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyhhmmss");
        String pdfName = "pdfdemo.pdf"
                + sdf.format(Calendar.getInstance().getTime()) + ".pdf";

// all created files will be saved at path /sdcard/PDFDemo_AndroidSRC/
        String path = Environment.getExternalStorageDirectory() + "/download/";
        File outputFile = new File(path, pdfName);

        try {
            outputFile.createNewFile();
            OutputStream out = new FileOutputStream(outputFile);
            document.writeTo(out);
            document.close();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
